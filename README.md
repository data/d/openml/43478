# OpenML dataset: Gold-Rate-History-in-TamilNadu-(2006-2020)

https://www.openml.org/d/43478

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
As you all know that, as per the observation of economists, according to the current trend, it seems that the yellow metal is performing better as an investment option in comparison to mutual funds, equities, real estate, and fixed deposits. The weak global economic outlook for the entire year is might be the reason why gold prices are surging. The yellow metal is considered as a financial instrument that does not erode in valuation during periods of economic turbulence. Many global investors are looking for safer investment options including gold as fears over a recession continue to grow.
Therefore here, need to forecast the price of the Gold in the future based on trend or seasonality using historical data from Jan 2006 to Sep 2020.  The historical data has from Jan 2006 to Sep 2020.
Problem
To Predict or forecast the gold price in the near future. Hence, it would help Indian people aware of when to buy gold for their investments.
Data
The data contains the following fields,

Date, 
Country,
State,
City,
Pure Gold (24 k), Priced indicated in INR
Standard Gold (22 k), Priced indicated in INR

The dataset will be updated soon for other states as well in India.
Acknowledgements
The dataset has scraped from www.livechennai.com. Gold Prices indicated in this data makes no guarantee or warranty on the accuracy or completeness of the data provided.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43478) of an [OpenML dataset](https://www.openml.org/d/43478). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43478/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43478/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43478/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

